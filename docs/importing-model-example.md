## Example for importing and rendering a custom model

### 1 - Finding an animated model
First, you'll need an animated model to import. For the purpose of this example, I'll be using a simple walking animation of "Paladin J Nordstrom" from [Mixamo.com](https://www.mixamo.com/)

![image](./assets/images/import-example-1.png)

!!! note
    When preparing your animated model, make sure it is animated **In Place**, you don't want your sprite character walking off-screen mid-animation!

### 2 - Importing the model in blender
Go to `File->Import->FBX.fbx` and select your file:
![image](./assets/images/import-example-2.png)

Now, the animated Pladin I just imported is just slapped in the scene like this:
![image](./assets/images/import-example-3.png)

### 3 - Preparing the object
First, I hide the existing armature (`dinkArmature`) and object (`rdai`) from rendering and showing up in the viewport. Then, I move the freshly imported Armature to be a child of ModelRotator (Hold Shift and Drag the imported Armature object). 

I also hit the `Reload Action List` button in the `Dink Render` panel.

The result looks like this:

![image](./assets/images/import-example-4.png)

### 4 - A test render
I hit F12 for a quick test render, the result looks like this:
![image](./assets/images/import-example-5.png)

The model doesn't quite fit in the sprite image! I adjust the **Sprite Ortographic Scale** to about 2.2 and hit F12 to try again. Now the sprite fits, but I go to the Compositing tab to check the in-game preview comparison to get a better sense of the scale.

Looking over the "In Game Preview Example" node, I select the Preview node. I then adjust the **Sprite Ortographic Scale** further and check how it looks in the preview.

![image](./assets/images/import-example-6.png)

!!! warning
    Make sure you have `Backdrop` enabled in Compositing view or else you won't see any preview at all!

!!! warning
    You need to hit F12 to render a frame every time to update the preview image!

!!! note
    You can zoom the preview in with ALT+V, or zoom out with V. With the node selected, you can also drag the preview window around by the X in at its center.

After some trial and error, I find the sprite looks good with the **Sprite Ortographic Scale** set to 2.5.

### 5 - More camera adjustments

Now, I'm not quite done here yet! I also test other sprite orientations. Using the quick rotation commands added to the Viewport in the `Dink` panel I rotate the sprite and hit F12 again to do a test render.

![image](./assets/images/import-example-7.png)

It seems that the sprite isn't centered quite right and for the 7/8/9 directions the top gets cut off a bit. So I raise the camera on the Z axis a bit.

![image](./assets/images/import-example-8.png)

### 6 - Finally, rendering the sprite
I disable the existing `idle` and `push` actions in the Scene actions list, and leave the `Armature|mixamo.com|Layer0` action enabled.

!!! Warning
    Don't forget, if you haven't already, you need to hit the `Reload action list` button in the `Dink Render` panel, to get the newly imported action to show up in the list!

All in all, my final settings look like this:

![image](./assets/images/import-example-9.png)

As of DBT V1.3, after pressing one of the render buttons, you can now see the render progress.

![image](./assets/images/import-example-10.png)

### 7 - The result
After waiting a while for the rendering script to do its thing, the sprites are ready:

![image](./assets/images/import-example-11.png#only-light)
![image](./assets/images/import-example-11-dark.png#only-dark)