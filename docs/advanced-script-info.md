In short, the `customRender.py` script creates a custom panel called **Dink Render** in the `Output -> Properties` page. It populates the panel with the various custom render properties and allows you to run the automatic-rendering function at the click of a button.

!!! note "The Script"
    I have included a copy of the [**customRender.py**](./assets/customRender.py) script here, but you should check the one included in the blender file itself in case I forget to update this one.

I won't go into too much detail about all the Blender UI related code, you can find plenty of tutorials for that either built into Blender itself or by searching the internet.

I won't go into detail line by line either as that would be quite borring and useless.

I'll just go over each class and important function and talk about them a bit.

## Useful blender python tutorial sources
As I'm quite new to python and especially blender specific python api, here are some of the useful resources I used while writing the script:
+ [Official Blender API documentation](https://docs.blender.org/api/current/index.html)
+ [Blender python tutorials from "sinestesia.co", ](https://sinestesia.co/blog/tutorials/using-uilists-in-blender/)
+ [Blender python tutorials from "B3D - Interplanety", by Nikita](https://b3d.interplanety.org/en/dynamically-setting-the-max-and-min-values-%E2%80%8B%E2%80%8Bfor-a-custorm-property-in-the-blender-python-api/)

## Blender Property Group classes


- **class DINK_Properties**

Inherits `bpy.types.PropertyGroup` and neatly stores all the custom Dink Render related properties.

Some of the properties trigger refreshing the compositing node tree and other scene data upon changing.

!!! note "EnumProperty"

    For some odd reason, the values of `bpy.props.EnumProperty` types do not get restored correctly upon reloading the .blend file. It seems that they get a random value each time! I am not sure if this is because of the properties themselves, or the combo box ui elements used to represent them.

    As a workaround, I have duplicated them using `bpy.props.StringProperty`. When the enum type properties change, they are also saved in the string properties and then upon registering the script, I also restore the enum values from the string values.
  
  
- **class DINK_ListItems**

Inherits `bpy.types.PropertyGroup`, used to store the custom properties for the action list in the UI.



## Blender UI classes

- **class DINK_UL_MyList**

Inherits `bpy.types.UIList`, a simple custom list with item names and checkboxes.


- **class DINK_PT_MyPanel**

Inherits `bpy.types.Panel`, defines the custom **Dink Render** panel in the `Output` properties panel zone.


- **class DINK_PT_RotatePanelView3D** and **class DINK_PT_RotatePanelNodeEditor**

Inherits `bpy.types.Panel`, defines the custom **Dink** panel in the `Tool` tab of the 3D Viewport and `Tool` tab of compositing.

The rotate commands in compositing also perform a render for the current frame in order to update the compositing preview.


## Blender Operator classes

- **class DINK_OP_RefreshActionList**

Inherits `bpy.types.Operator`, custom Blender Operator for refreshing the Action list.


- **class DINK_OP_RefreshCompositing**

Inherits `bpy.types.Operator`, custom Blender Operator for updating the Compositing nodes without rendering.


- **class DINK_OP_Rotate**

Inherits `bpy.types.Operator`, custom Blender Operator for rotating the object via the `ModelRotator` parent, in one of the 8 Dink directions.


- **class DINK_OP_CustomRenderCancelModal**

Inherits `bpy.types.Operator`, custom Blender Operator for aborting render process.


- **class DINK_OP_CustomRenderModal**

Inherits `bpy.types.Operator`, custom Blender Operator for starting the custom rendering process.

Is hardcoded to work as a modal operator. Upon each execution of the modal function, it renders a single sprite frame to a file. This is repeated using a timer until all the frames are rendered.

Calls `DINK_PropsUpdateCompositingNodes` to update the Compositing nodes and calls `DINK_RenderModel` to actually render stuff.

## Various helper functions

- **DINK_PropsUpdateRenderSettings**

This function is for updating various rendering properties based on the current `dinkProps` values. It is called whenever a Dink render property changes or when the rendering process is started.


- **DINK_PropsUpdateCompositingNodes**

This is responsible for updating the Compositing nodes. It reads the current scene from the blender context, gets the custom `dinkProps` custom properties. Based on those properties, it switches node paths for PNG or BMP rendering. It also updates some node threshold values based on the custom Dink properties' values.


- **DINK_PropsInitializeActionList**

This function initializes or reinitializes the list of actions in the custom dink properties...


## Render functions and classes

- **class Dink_TargetRenderContext**

Helper class that keeps track of the current render state for the modal rendering operator.

It gets instantiated as a global variable. It is not saved in the scene data, instead it lives only as long as the blender session is active.

Must call `prepareRender` which prepares some metadata for the sprite frames that need to be rendered. Afterward, if its `isValid` property is `True`, `renderAll` can be called to render all the sprites at once or `renderSingleFrame` can be called to render a single frame. `renderSingleFrame` can be called repeatedly until the `isDone` property is `True`; 


- **class DINK_TargetRenderData**

Keeps some metadata for the frames that need to be rendered for a given action.


- **class DINK_TargetData**

Responsible for manipulating / rendering the model.

Upon initializing, locates the target `Armature`, `ModelRotator` object and `Action` to be animated. If everything is good, the `isValid` property will be `True`. Otherwise, the last error message will be in the `message` property.

After initializing, it can be used to rotate the `ModelRotator` or load the `Action` into the target `Armature`.

If it `isValid`, then `initRenderFrameList` can be called to initialize a list of metadata for each frame that needs to be rendered for its action. This goes through every selected Dink direction, and for each frame of the action it prepares the angle that the model needs to be rotated, the frame index, and the file name for the final render output. This data is then used by the `Dink_TargetRenderContext` class to render the frames.


- **Direction definitions**

There are some helpful functions for generating definitions for the Dink sprite orientation directions. Either as a dictionary, or as a list.
- `DINK_GetDirectionsDict`
- `DINK_GetDirectionsAll`
- `DINK_GetDirections2468`
- `DINK_GetDirections1379`

Helper functions that return definitions of the Dink directions:

| Dink Direction ID  | Angle      | Cardinal Direction |
|--------------------|------------|--------------------|
| 1                  | 315        | South-west         |
| 2                  | 0          | South              |
| 3                  | 45         | South-east         |
| 4                  | 270        | West               |
| 6                  | 90         | East               |
| 7                  | 225        | North-west         |
| 8                  | 180        | North              |
| 9                  | 135        | North-east         |

See here for more info: [https://dinkcreference.netlify.app/functions/sp-dir.html#sp-dir](https://dinkcreference.netlify.app/functions/sp-dir.html#sp-dir)
