## Dink Render Settings

![image](./assets/images/custom-render-menu.png)

Here is a list of all the custom **Dink Render** properties and what they do.

### Output Path
Self-explanatory. This is the base output directory where the script will save the rendered sprites. The actual sprite images will actually be saved in subdirectories based on the action names.

### Name Format
Select the file name format to be one of the following:

* `4 letter prefix`, `1 digit - direction`, `dash`, `2 digit - frame number`
* `5 letter prefix`, `1 digit - direction`, `2 digit - frame number`

Example: `sprt2-01.bmp` vs `sprit201.bmp`

### Sprite Prefix
All saved sprite images will have this 4/5 letter prefix to their name. Dink sprite names can be up to 8 characters long, but the last two are reserved for the frame ID, and we use one more for the direction ID digit.

The name of the *Sprite Prefix* gets limited to 4 or 5 characters based on the *Name Format* property.

### Sprite Size X/Y
This sets the `Output->Format->Resolution X/Y` properties when rendering and defines the size of the sprite image.

!!! warning
    Do not set the `Output->Format->Resolution X/Y` properties manually as they will get overwritten!

### Sprite Ortographic Scale
This sets the `Camera->Lens->Ortographic Scale` property, which determines how big the sprite is in the rendered image.

A larger value results in a sprite that is zoomed out more, and a smaller value zooms in on the sprite.

!!! warning
    Do not set the `Camera->Lens->Ortographic Scale` property manually as it will get overwritten!

### Sprite Render Samples
This sets the `Render->Render->Samples` property. The higher the value, the smoother the sprite will be. The lower the value, the more pixelated the sprite will be.

!!! warning
    Do not set the `Render->Render->Samples` property manually as it will get overwritten!


### Output Format
You can select the classic BMP or PNG for more modern Dink engines that support PNG format. If you're using PNG, the sprite will have transparency, but the shadow will still be the good old grid shadow.

### BMP Edge Filter Threshold
Ranges from 0.001 to 1.000, default value is 0.05

When saving BMP, you need to have hard pixels on the edges, not semi-transparent ones. This threshold changes a parameter in the compositing process that affects the edge pixel "hardening". Basically, the higher the value, the more of the sprite will be cut off when calculating edges. If you set it to 1.0 you will pretty much only end up rendering the shadow, and not the object.

!!! note
    This property is only available when using BMP format!

### Shadow Filter Threshold
Ranges from 0.001 to 1.000, default value is 0.92

Determines how much of the Shadow Catcher data is actually part of the dink pixel grid shadow. The lower the value, the more pixels get ignored. If this is too close to 1.0 you will end up with pretty much everything being part of the shadow!

Adjust this if needed.

### Render Normal Shadow

This option is available when using PNG format. If enabled, it will render a regular plain normal shadow instead of the classic Dink grid pattern shadow.

!!! note
    This property is only available when using PNG format!

### Normal Shadow Amplifier
Ranges from 0.001 to 8.000, default value is 0.8

This option is available when using normal shadows with the PNG format. Increasing the value makes the normal shadow darker, while decreasing it makes it lighter. Adjust as desired.

!!! note
    This property is only available when using PNG format with normal shadow enabled!

### Normal Shadow Filter Threshold
Ranges from 0.001 to 1.000, default value is 0.02

This option is available when using normal shadows with the PNG format. It filters out some low value noise/parasite pixels from the shadow. Adjust if needed.

!!! note Only for PNG
    This property is only available when using PNG format with normal shadow enabled!

### Scene actions & Refresh Action List
This is a list of what actions from your blender file you want to render. You can untick the checkboxes if you don't want to render them.

!!! warning "Warning"
    Unfortunately, for now, the script does not automatically update the list when you create/import new actions, it only populates the list upon running the script. So if you added new actions, click the "Refresh Action List" to update it.

### Render Sprites
There are 3 render buttons here.

- **Render All Directions** - This does exactly what you think it does.
- **Diagonal Only (1379)** - This renders only diagonal directions, aka directions 1, 3, 7 and 9
- **Straight Only (2468)** - This renders only the straight/horizontal/vertical directions, aka directions 2, 4, 6, 8

After you start rendering, you can see the progress:
![image](./assets/images/render-progress.png)

!!! note "Progress Bar"
    
    The progress bar is only available in Blender V4.0 or newer. In Blender V3.6 you will only see the progress text.



!!! note "In Game Preview Example"

    To preview what the sprite would look like in-game, you can do a quick render with F12 to refresh the Compositing previews, then go to the Compositing workspace and check the `Preview` nodes of the `In Game Preview Example` group. They should show you what the sprite would look like in a DinkHD rendered screen from the Periculo Island DMOD, like so:
    ![image](./assets/images/compositing-8-preview.png)
    
    You can also adjust the sprite position using the **Translate** nodes in the group.
