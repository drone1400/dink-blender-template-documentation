import bpy
import os
import math

from bl_ui.generic_ui_list import draw_ui_list

# ----------------------------------------------------------------
# Blender - PropertyGroup classes
# ----------------------------------------------------------------

class DINK_ListItems(bpy.types.PropertyGroup):
    """This Property Group defines the properties for the scene actions List"""

    name : bpy.props.StringProperty(
        name="Action Name",
        description="The name of the action to be used for animating during rendering",
        default="None")


    enable : bpy.props.BoolProperty(
        name="Enable",
        description="If True, will use action of this name in rendering",
        default=True)


# NOTE: it seems that the get and set overrides can not be defined within the class itself...
# weird...

def get_outPrefix(self):
    return self.get("outPrefix",0)
def set_outPrefix(self,value):
    # limit max length depending on selected sprite prefix format!
    if (self.outSpriteNameFormat == "prefix4"):
        # limit to 4 characters
        self["outPrefix"]=value[:4]
    if (self.outSpriteNameFormat == "prefix5"):
        # limit to 5 characters
        self["outPrefix"]=value[:5]


class DINK_Properties(bpy.types.PropertyGroup):
    """Property group that holds all of our custom Dink Render related properties"""

    def update_sceneStuff(self, context):
        action = self.actionList[self.actionListIndex]
        target = DINK_TargetData(context,action.name)

        if (target.isValid == False):
            return

        print("Updating armature action...")
        print(target.message)
        target.updateArmatureAction(context)

    def update_outSpriteNameFormat(self, context):
        # save duplicate value...
        self.outSpriteNameFormat2 = self.outSpriteNameFormat
        # when outSpriteNameFormat changes, set outPrefix value again to enforce max length
        set_outPrefix(self,self["outPrefix"])

    def update_outFormat(self, context):
        # save duplicate value...
        self.outFormat2 = self.outFormat
        DINK_PropsUpdateCompositingNodes(context)
        DINK_PropsUpdateRenderSettings(context)

    def update_compositingProperty(self, context):
        DINK_PropsUpdateCompositingNodes(context)
        DINK_PropsUpdateRenderSettings(context)

    def restoreEnumValues(self):
        # for some reason the enum properties seem to not get restored properly when opening the blend file...
        # this restores them from the duplicate string properties...
        print("Restoring enum values...")

        self.outSpriteNameFormat = self.outSpriteNameFormat2
        print("restored outSpriteNameFormat: " + str(self.outSpriteNameFormat2))

        self.outFormat = self.outFormat2
        print("restored outFormat: " + str(self.outFormat2))

    # ------------------------------------------------
    # output file settings

    outPath : bpy.props.StringProperty(
        name="Output Path",
        description="Base directory for the custom Dink sprite render",
        default="C:\\dink_render\\",
        maxlen=260)

    outPrefix : bpy.props.StringProperty(
        name="Sprite Prefix",
        description="Four letter prefix for the sprite name",
        default="SPRT-",
        maxlen=5,
        # use custom get/set to enforce dynamic max length based on outSpriteNameFormat
        # i don't know if there's a way to change the actual "maxlen" property at runtime, hmm...
        get=lambda self: get_outPrefix(self),
        set=lambda self, value: set_outPrefix(self, value))

    outSpriteNameFormat : bpy.props.EnumProperty(
        name="Name Format",
        description="Select BMP or PNG with transparency format...",
        items={
            ("prefix4", "sprt2-01.bmp", "Example: sprt2-01.bmp, sprt2-02.bmp, etc"),
            ("prefix5", "sprit201.bmp", "Example: sprit201.bmp, sprit202.bmp, etc"),
        },
        default="prefix4",
        update=update_outSpriteNameFormat)

    outSpriteNameFormat2 : bpy.props.StringProperty(
        name="Name Format (internal)",
        description="Internal duplicate value for outSpriteNameFormat",
        default="prefix4")

    # ------------------------------------------------
    # render settings
    #

    #render resolution_x
    outSpriteSizeX : bpy.props.IntProperty(
        name="Sprite Size X",
        description="Sprite image width in pixels",
        default=110,
        min=4,
        max=2048,
        update=update_compositingProperty)

    #render resolution_y
    outSpriteSizeY : bpy.props.IntProperty(
        name="Sprite Size Y",
        description="Sprite image height in pixels",
        default=110,
        min=4,
        max=2048,
        update=update_compositingProperty)

    # camera ortographic scale
    outSpriteScale : bpy.props.FloatProperty(
        name="Sprite Ortographic Scale",
        description="This adjusts the camera lens ortographic scale factor. NOTE: higher value means sprite will be zoomed out more!",
        min=0.001,
        max=8192.000,
        soft_min=0.000,
        soft_max=8192.000,
        step=1,
        precision=3,
        default=1.5,
        update=update_compositingProperty)

    #cycles render samples
    outSpriteSamples : bpy.props.IntProperty(
        name="Sprite Render Samples",
        description="The lower the value, the more pixelated and noisy the resulting image will be",
        default=128,
        min=4,
        max=16384,
        update=update_compositingProperty)

    #render save as PNG or BMP
    outFormat : bpy.props.EnumProperty(
        name="Output Format",
        description="Select BMP or PNG with transparency format...",
        items={
            ("BMP", "BMP", "Save as BMP"),
            ("PNG", "PNG", "Save as PNG"),
        },
        default="BMP",
        update=update_outFormat)


    outFormat2 : bpy.props.StringProperty(
        name="Output Format (internal)",
        description="Internal duplicate value for outFormat",
        default="BMP")


    # ------------------------------------------------
    # compositing settings
    #

    compEdgeFilterThreshold : bpy.props.FloatProperty(
        name="BMP Edge Filter Threshold",
        description="Threshold for filtering sprite edges based on alpha...",
        min=0.001,
        max=1.000,
        soft_min=0.000,
        soft_max=1.000,
        step=1,
        precision=3,
        default=0.1,
        update=update_compositingProperty)

    compDinkShadowThreshold : bpy.props.FloatProperty(
        name="Dink Shadow Filter Threshold",
        description="Threshold for filtering how much of the shadow gets converted...",
        min=0.001,
        max=1.000,
        soft_min=0.000,
        soft_max=1.000,
        step=1,
        precision=3,
        default=0.92,
        update=update_compositingProperty)

    compNormalShadowThreshold : bpy.props.FloatProperty(
        name="Normal Shadow Filter Threshold",
        description="Threshold for filtering the shadow...",
        min=0.001,
        max=1.000,
        soft_min=0.000,
        soft_max=1.000,
        step=1,
        precision=3,
        default=0.02,
        update=update_compositingProperty)

    compNormalShadowAmplifier : bpy.props.FloatProperty(
        name="Normal Shadow amplifier",
        description="Multiplies shadow by this value making it darker (or lighter)...",
        min=0.001,
        max=8.000,
        soft_min=0.000,
        soft_max=1.000,
        step=1,
        precision=3,
        default=1.10,
        update=update_compositingProperty)

    compPngNormalShadow : bpy.props.BoolProperty(
        name="Render normal shadow",
        description="If checked, will render a normal shadow instead of the Dink grid pattern shadow",
        default=True,
        update=update_compositingProperty)

    # ------------------------------------------------
    # other settings
    #

    renderDirections : bpy.props.EnumProperty(
        name="Render Directions",
        description="",
        items={
            ("dir_all", "All", "Render all directions"),
            ("dir_1379", "1379", "Render diagonal"),
            ("dir_2468", "2486", "Render straight"),
        },
        default="dir_all")

    actionList : bpy.props.CollectionProperty(
        type=DINK_ListItems)

    actionListIndex : bpy.props.IntProperty(
        name="Index for action list",
        default=0,
        update=update_sceneStuff)


# ----------------------------------------------------------------
# Blender - Custom UI elements
# ----------------------------------------------------------------

# NOTE: this was very useful while making the custom list object in this script!...
# https://sinestesia.co/blog/tutorials/using-uilists-in-blender/


class DINK_UL_MyList(bpy.types.UIList):
    """Creates my custom action list"""

    # define function for drawing list items
    def draw_item(self, context, layout, data, item, icon, active_data, active_propname, index):
        #if self.layout_type in {"DEFAULT", "COMPACT", "GRID"}:

        # simply draw a label using the name, and a checkbox property next to it
        layout.label(text=item.name)
        layout.prop(item,"enable")

class DINK_PT_MyRenderPanel(bpy.types.Panel):
    """Creates a custom panel for rendering Dink Sprites in the Output properties zone"""

    # set panel title
    bl_label = "Dink Render"

    # set panel internal id thing
    bl_idname = "DINK_PT_MyPanel"

    # these define where the panel is created...
    bl_space_type = "PROPERTIES"
    bl_region_type = "WINDOW"
    bl_context = "output"

    # define function for drawing the panel
    def draw(self, context):
        # get layout
        layout = self.layout

        # get current scene
        scene = context.scene

        # get current Dink render properties
        dinkProps = scene.dinkProps

        # send simple dinkProps properties to layout...
        layout.prop(dinkProps, "outPath")
        layout.prop(dinkProps, "outSpriteNameFormat")
        layout.prop(dinkProps, "outPrefix")
        layout.prop(dinkProps, "outSpriteSizeX")
        layout.prop(dinkProps, "outSpriteSizeY")
        layout.prop(dinkProps, "outSpriteScale")
        layout.prop(dinkProps, "outSpriteSamples")
        layout.prop(dinkProps, "outFormat")

        if (dinkProps.outFormat == "BMP"):
            box=layout.box()
            box.prop(dinkProps, "compEdgeFilterThreshold")
            box.prop(dinkProps, "compDinkShadowThreshold")
        else:
            box=layout.box()
            box.prop(dinkProps, "compPngNormalShadow")
            if (dinkProps.compPngNormalShadow == False):
                box.prop(dinkProps, "compDinkShadowThreshold")
            else:
                box.prop(dinkProps, "compNormalShadowAmplifier")
                box.prop(dinkProps, "compNormalShadowThreshold")

        # action list stuff
        box=layout.box()
        # label for the action list
        box.label(text="Scene actions:")
        # create a list using the dinkProps.actionList and dinkProps.actionListIndex properties
        row = box.row()
        row.template_list("DINK_UL_MyList", "DinkRenderActionList",
                          dinkProps, "actionList",
                          dinkProps, "actionListIndex")
        # button for refreshing action list
        box.operator(DINK_OP_RefreshActionList.bl_idname, text="Reload Action List")

        # buttons for executing custom render operation

        box=layout.box()
        box.label(text="Render sprites:")
        if (gDinkCtx.isValid == True and gDinkCtx.isDone == False):
            textMsg = "Working... " + str(gDinkCtx.frameProgress) + "/" + str(gDinkCtx.frameCount)
            box.operator(DINK_OP_CustomRenderCancelModal.bl_idname, text="Abort rendering!")

            if (gDinkCtx.canUseCoolProgressBar == False):
                box.label(text=textMsg)
                if (gDinkCtx.lastSingleFrameDirection != None):
                    box.label(text=gDinkCtx.lastSingleFrameDirection)
                if (gDinkCtx.lastSingleFrameFile != None):
                    box.label(text=gDinkCtx.lastSingleFrameFile)

            else:
                if (gDinkCtx.frameCount == 0):
                    context.window_manager.dinkRenderProgress = 0
                else:
                    context.window_manager.dinkRenderProgress = float(gDinkCtx.frameProgress) / float(gDinkCtx.frameCount)
                box.progress(
                    factor=context.window_manager.dinkRenderProgress,
                    type="BAR",
                    text=textMsg)
                if (gDinkCtx.lastSingleFrameDirection != None):
                    box.label(text=gDinkCtx.lastSingleFrameDirection)
                if (gDinkCtx.lastSingleFrameFile != None):
                    box.label(text=gDinkCtx.lastSingleFrameFile)
        else:
            box.label(text="Choose what directions to render")
            renderAll = box.operator(DINK_OP_CustomRenderModal.bl_idname, text="Render All Directions")
            renderAll.renderDirections = "dir_all"
            render1357 = box.operator(DINK_OP_CustomRenderModal.bl_idname, text="Diagonal Only (1379)")
            render1357.renderDirections = "dir_1379"
            render2468 = box.operator(DINK_OP_CustomRenderModal.bl_idname, text="Straight Only (2468)")
            render2468.renderDirections = "dir_2468"

def DINK_DrawRotateButtonsCommon(layout, renderAfterRotate):
    # label for the action list
    layout.label(text="Model Rotator:")

    # create a column layout
    col = layout.column()

    row = col.row()
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="7")
    rotate.rotationIndex = 7
    rotate.renderAfterRotate = renderAfterRotate
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="8")
    rotate.rotationIndex = 8
    rotate.renderAfterRotate = renderAfterRotate
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="9")
    rotate.rotationIndex = 9
    rotate.renderAfterRotate = renderAfterRotate

    row = col.row()
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="4")
    rotate.rotationIndex = 4
    rotate.renderAfterRotate = renderAfterRotate
    row.label(text="")
    #rotate = row.operator(DINK_OP_Rotate.bl_idname, text=" ")
    #rotate.rotationIndex = 5
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="6")
    rotate.rotationIndex = 6
    rotate.renderAfterRotate = renderAfterRotate

    row = col.row()
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="1")
    rotate.rotationIndex = 1
    rotate.renderAfterRotate = renderAfterRotate
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="2")
    rotate.rotationIndex = 2
    rotate.renderAfterRotate = renderAfterRotate
    rotate = row.operator(DINK_OP_Rotate.bl_idname, text="3")
    rotate.rotationIndex = 3
    rotate.renderAfterRotate = renderAfterRotate


class DINK_PT_RotatePanelNodeEditor(bpy.types.Panel):
    """Creates a custom panel for rotating the model from the Compositing view..."""

    # set panel title
    bl_label = "Dink"

    # set panel internal id thing
    bl_idname = "DINK_PT_RotatePanelNodeEditor"

    # these define where the panel is created...
    bl_space_type = "NODE_EDITOR"
    bl_region_type = "UI"
    bl_category = "Tool"

    # define function for drawing the panel
    def draw(self, context):
        DINK_DrawRotateButtonsCommon(self.layout, True)


class DINK_PT_RotatePanelView3D(bpy.types.Panel):
    """Creates a custom panel for rotating the model from the 3D view..."""

    # set panel title
    bl_label = "Dink"

    # set panel internal id thing
    bl_idname = "DINK_PT_RotatePanelView3D"

    # these define where the panel is created...
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
    bl_category = "Tool"

    # define function for drawing the panel
    def draw(self, context):
        DINK_DrawRotateButtonsCommon(self.layout, False)



# ----------------------------------------------------------------
# Blender - Operator classes
# ----------------------------------------------------------------

class DINK_OP_RefreshActionList(bpy.types.Operator):
    """Custom operator for refreshing action list"""
    # operator title
    bl_label = "Refresh Action List"
    # operator id
    bl_idname = "wm.dinkoprefreshactionlist"

    # define function for executing the operator
    def execute(self, context):
        DINK_PropsInitializeActionList(context)
        return {"FINISHED"}


class DINK_OP_RefreshCompositing(bpy.types.Operator):
    """Custom operator for refreshing compositing without rendering"""
    # operator title
    bl_label = "Refresh Compositing"
    # operator id
    bl_idname = "wm.dinkoprefreshcompositing"

    # define function for executing the operator
    def execute(self, context):
        DINK_PropsUpdateRenderSettings(context)
        DINK_PropsUpdateCompositingNodes(context)

        # clear any action that might be associated with the rotator as that can mess up rendering by
        # overriding the rotation we set
        target = DINK_TargetData(context,action.name)
        if (target.isValid == False):
            return {"FINISHED"}
        target.clearRotatorAction(context)
        return {"FINISHED"}


class DINK_OP_Rotate(bpy.types.Operator):
    """Custom operator for rotating the model along the 8 Dink directions"""
    # operator title
    bl_label = "Refresh Compositing"
    # operator id
    bl_idname = "wm.dinkoprotate"

    # this property defines the dink direction... 5 is ignored
    rotationIndex : bpy.props.IntProperty(
        name="Dink Rotation Index",
        default=1,
        min=1,
        max=9)

    renderAfterRotate : bpy.props.BoolProperty(
        name="Render after rotating",
        default=False)

    # define function for executing the operator
    def execute(self, context):
        dict = DINK_GetDirectionsDict();
        angleDef = dict[self.rotationIndex]

        if (angleDef == None):
            return {"FINISHED"}

        dinkProps = context.scene.dinkProps
        action = dinkProps.actionList[dinkProps.actionListIndex]
        target = DINK_TargetData(context,action.name)

        if (target.isValid == False):
            return {"FINISHED"}

        print("Setting direction... " + angleDef[2])
        target.rotateTargetZ(angleDef[0])
        target.clearRotatorAction(context)

        if (self.renderAfterRotate == True):
            # this is useful for refreshing compositing view immediately
            bpy.ops.render.render( animation=False, write_still=False)

        return {"FINISHED"}


class DINK_OP_CustomRenderCancelModal(bpy.types.Operator):
    """Custom operator for aborting the custom Dink Render process"""
    # operator title
    bl_label = "Custom Render Cancel Modal"

    # operator id
    bl_idname = "wm.dinkopcustomrendercancelmodal"

    # define function for executing the operator
    def execute(self, context):
        # just set this to false and everything should sort itself i think
        gDinkCtx.isValid = False
        return {"FINISHED"}


class DINK_OP_CustomRenderModal(bpy.types.Operator):
    """Custom operator for starting the custom Dink Render process"""
    # operator title
    bl_label = "Custom Render (Modal)"

    # operator id
    bl_idname = "wm.dinkopcustomrendermodal"

    renderDirections : bpy.props.StringProperty(
        name="Render Directions",
        default="dir_all")

    renderModal : bpy.props.BoolProperty(
        name="Render Modal",
        default=True)

    myTimer = None

    def modal(self, context, event):
        [a.tag_redraw() for a in context.screen.areas]

        if (gDinkCtx.isValid != True):
            return {"FINISHED"}

        gDinkCtx.renderSingleFrame(context)

        if (gDinkCtx.isDone == True):
            return {"FINISHED"}

        return {"PASS_THROUGH"}


    # define function for executing the operator
    def execute(self, context):
        dinkProps = context.scene.dinkProps
        dinkProps.renderDirections = self.renderDirections

        # first update the render and compositing...
        DINK_PropsUpdateRenderSettings(context)
        DINK_PropsUpdateCompositingNodes(context)

        gDinkCtx.prepareRender(context)
        print ("prepared...")
        print (gDinkCtx.isValid)
        if (self.renderModal == False):
            gDinkCtx.renderAll(context)
            return {"FINISHED"}
        else:
            self.myTimer = context.window_manager.event_timer_add(0.1, window=context.window)
            context.window_manager.modal_handler_add(self)
            return {"RUNNING_MODAL"}


# ----------------------------------------------------------------
# helper functions
# ----------------------------------------------------------------

def DINK_PropsUpdateRenderSettings(context):
    """This function updates render settings based on current dinkProps values"""

    # get current scene
    scene = context.scene

    # get current Dink render properties
    dinkProps = scene.dinkProps

    scene.render.resolution_x = dinkProps.outSpriteSizeX
    scene.render.resolution_y = dinkProps.outSpriteSizeY
    scene.cycles.samples = dinkProps.outSpriteSamples

    # check if saving as PNG or BMP
    if dinkProps.outFormat == "PNG":
        #NOTE: compositing gets updated elsewhere to enable transparent background...
        scene.render.image_settings.file_format="PNG"
        scene.render.image_settings.color_mode="RGBA"
    else :
        scene.render.image_settings.file_format="BMP"
        scene.render.image_settings.color_mode="RGB"

        # set camera ortographic scale
    bpy.data.objects["Camera"].data.ortho_scale = dinkProps.outSpriteScale


def DINK_PropsUpdateCompositingNodes(context):
    """This function updates compositing nodes based on current dinkProps values"""

    myNodes = context.scene.node_tree.nodes
    myLinks = context.scene.node_tree.links
    dinkProps = context.scene.dinkProps

    # for debug, printing all the nodes...
    #for a in myNodes: print(a.name)

    if dinkProps.outFormat == "PNG":
        # get output node
        nodeOut = myNodes["DinkFinal.Output"]
        # make sure to use alpha
        nodeOut.use_alpha = True
        # get png node
        nodePng = myNodes["DinkRenderPng.AlphaOver"]

        # switch compositing output for PNG mode
        myLinks.new(
            nodeOut.inputs["Image"],     #input node socket
            nodePng.outputs["Image"],    #output node socket
            verify_limits = True)

        # also switch compositing for preview nodes
        preview = myNodes["DinkPreview.Translate"]
        myLinks.new(
            preview.inputs["Image"],     #input node socket
            nodePng.outputs["Image"],    #output node socket
            verify_limits = True)

        if (dinkProps.compPngNormalShadow == True):
            nodeShadow = myNodes["DinkShadowNormal.Crop"]
            myLinks.new(
                nodePng.inputs[1],              #input node socket
                nodeShadow.outputs["Image"],    #output node socket
                verify_limits = True)
        else:
            nodeShadow = myNodes["DinkRenderPng.SetAlpha"]
            myLinks.new(
                nodePng.inputs[1],              #input node socket
                nodeShadow.outputs["Image"],    #output node socket
                verify_limits = True)
    else :
        # get output node
        nodeOut = myNodes["DinkFinal.Output"]
        # disable alpha
        nodeOut.use_alpha = False
        # get bmp node
        nodeBmp = myNodes["DinkRenderBmp.AlphaOverWhite"]
        # switch compositing for BMP mode
        myLinks.new(
            nodeOut.inputs["Image"],     #input node socket
            nodeBmp.outputs["Image"],    #output node socket
            verify_limits = True)

        # also switch compositing for preview nodes
        preview = myNodes["DinkPreview.Translate"]
        nodeBmp2 = myNodes["DinkRenderBmp.AlphaOver"]
        myLinks.new(
            preview.inputs["Image"],     #input node socket
            nodeBmp2.outputs["Image"],    #output node socket
            verify_limits = True)


    # update edge filter threshold...
    # only really relevant for BMP but update always
    myNodes["DinkEdge.GreaterThan"].inputs[1].default_value = dinkProps.compEdgeFilterThreshold

    myNodes["DinkShadowGrid.GreaterThan"].inputs[1].default_value = dinkProps.compDinkShadowThreshold
    myNodes["DinkShadowNormal.GreaterThan"].inputs[1].default_value = dinkProps.compNormalShadowThreshold
    myNodes["DinkShadowNormal.Amplifier"].inputs[1].default_value =  dinkProps.compNormalShadowAmplifier

    #
    # fix for V4.2 translate node break...
    #

    # image size
    cols = dinkProps.outSpriteSizeX
    rows = dinkProps.outSpriteSizeY
    # initialize pixels
    pixels = [0.0] * (4 * rows * cols)
    # write white pixels
    pixelIndex = 0
    for row in range(rows):
        if (row % 2) == 1:
            isWhite = 0
        else:
            isWhite = 1
        for col in range(cols):
            if (col % 2) == isWhite:
                pixels[pixelIndex+0] = 1.0
                pixels[pixelIndex+1] = 1.0
                pixels[pixelIndex+2] = 1.0
                pixels[pixelIndex+3] = 1.0
            pixelIndex = pixelIndex + 4

            # initialize image object
    # remove old image
    oldImage = bpy.data.images.get("MyDbtGridPatternImage")
    if (oldImage != None):
        bpy.data.images.remove(oldImage)
    # create new image
    myImage = bpy.data.images.new("MyDbtGridPatternImage", width=cols, height=rows)
    myImage.pixels = pixels
    # for debug...
#    myImage.filepath_raw = "D:/myTestImage.png"
#    myImage.file_format = "PNG"
#    myImage.save()

    # set image in node
    myNodes["DinkShadowGrid.CheckerImage"].image = myImage


def DINK_PropsInitializeActionList(context):
    """
    Initializes the items in the context's dinkProps.actionList property
    """

    # get current Dink render properties
    dinkProps = context.scene.dinkProps

    # prepare a temporary dictionary
    temp = dict()

    # populate temporary dictionary with old values...
    for a in dinkProps.actionList:
        temp[a.name]=a.enable

    # clear values from list
    dinkProps.actionList.clear()

    # populate list from existing actions
    for a in bpy.data.actions:
        item = dinkProps.actionList.add()
        # item name is same as action name
        item.name = a.name
        # by default, enable rendering the action
        item.enable = True
        # if the action existed before, restore enable value
        if (temp.get(item.name) != None):
            item.enable = temp[item.name]



# ----------------------------------------------------------------
# Render stuff
# ----------------------------------------------------------------

class DINK_TargetRenderContext:
    targetList = []
    frameCount = 0
    frameProgress = 0
    currentTargetIndex = 0
    currentTargetFrameIndex = 0
    logFile = None
    isValid = False
    isDone = False
    canUseCoolProgressBar = False
    lastSingleFrameDirection = None
    lastSingleFrameFile = None

    def clear(self):
        self.targetList = []
        self.frameCount = 0
        self.frameProgress = 0
        self.currentTargetIndex = 0
        self.currentTargetFrameIndex = 0
        self.logFile = None
        self.isValid = False
        self.isDone = False
        self.lastSingleFrameDirection = None
        self.lastSingleFrameFile = None


    def prepareRender(self, context):
        # clear current state
        self.clear()

        # get current Dink render properties
        dinkProps = context.scene.dinkProps

        # read relevant properties
        filePrefix = dinkProps.outPrefix

        if dinkProps.outFormat == "PNG":
            # use PNG subfolder instead for base path
            pathBasePng = os.path.join(dinkProps.outPath, "png-sprites")
            # make sure path is absolute
            pathBase = os.path.abspath(pathBasePng)
        else :
            # use BMP subfolder for base path
            pathBaseBmp = os.path.join(dinkProps.outPath, "bmp-sprites")
            # make sure path is absolute
            pathBase = os.path.abspath(pathBaseBmp)

        # make base folder if it does not exist
        if not os.path.exists(pathBase):
            os.makedirs(pathBase)

        # sanity check - did we really make the folder?
        if not os.path.exists(pathBase):
            print("DINK Render Error - base path is invalid: ", pathBase)
            return

        nameMode = dinkProps.outSpriteNameFormat

        # get angles
        dinkAngles = DINK_GetDirectionsAll()
        if dinkProps.renderDirections == "dir_1379":
            dinkAngles = DINK_GetDirections1379()
        if dinkProps.renderDirections == "dir_2468":
            dinkAngles = DINK_GetDirections2468()

        # prepare log file
        logFileName = "RendScriptLog.txt"
        pathLog = os.path.join(pathBase,logFileName)
        self.logFile = open(pathLog, "w")

        # check each action in the dinkProps.actionList
        for a in dinkProps.actionList:
            # if the user enabled the action for rendering, do the thing
            if a.enable == True:

                #initialize target data
                target = DINK_TargetData(context, a.name)
                target.initRenderFrameList(context, pathBase, filePrefix, nameMode, dinkAngles)

                if (target.message != None):
                    self.logFile.write(target.message)
                if (target.isValid == True and
                    target.isRenderFrameListValid == True):
                    self.targetList.append(target)
                    self.frameCount = self.frameCount + len(target.renderFrameList)

        # all done and ready
        self.isValid = True

    def renderAll(self, context):
        if (self.isValid != True):
            return

        for target in self.targetList:
            # set the target's armature animation
            target.updateArmatureAction(context)
            # render each frame...
            for rd in target.renderFrameList:
                logFile.write("Rendering dir=" + rd.angleDef[3] + ", frame=" + str(rd.frameIndex) + ", " + rd.fileName + " \n")
                target.renderFrame(context, rd)
            # restore rotator to initial zero position
            target.rotateTargetZ(0)

        # close log at the end
        logFile.close()
        self.isDone = True

    def renderSingleFrame(self, context):
        if (self.isValid != True):
            return

        if (len(self.targetList) == 0):
            self.isDone = True

        # check if done
        if self.isDone == True:
            if (self.logFile != None):
                self.logFile.close()
            return

        # get current target
        target = self.targetList[self.currentTargetIndex]

        # check if need to update armature action
        if (self.currentTargetFrameIndex == 0):
            target.updateArmatureAction(context)
            # clear any action that might be associated with the rotator as that can mess up rendering by
            # overriding the rotation we set
            target.clearRotatorAction(context)

        # get current frame data and render it
        frameData = target.renderFrameList[self.currentTargetFrameIndex]
        msg = "Rendering dir=" + frameData.angleDef[3] + ", frame=" + str(frameData.frameIndex) + ", " + frameData.fileName + " \n"
        self.lastSingleFrameDirection = "dir=" + frameData.angleDef[3] + ", frame=" + str(frameData.frameIndex)
        self.lastSingleFrameFile = frameData.fileName
        print(msg)
        self.logFile.write(msg)
        target.renderFrame(context, frameData)

        # increment index
        self.currentTargetFrameIndex = self.currentTargetFrameIndex + 1
        self.frameProgress = self.frameProgress + 1;
        if (self.currentTargetFrameIndex >= len(target.renderFrameList)):
            # restore rotator to initial zero position
            target.rotateTargetZ(0)
            #time to move to next target!
            self.currentTargetIndex = self.currentTargetIndex + 1
            # reset current target frame
            self.currentTargetFrameIndex = 0
            # check if done
            if (self.currentTargetIndex >= len(self.targetList)):
                self.currentTargetIndex = 0
                self.isDone = True
                self.logFile.close()
                self.logFile = None
#end of class...

gDinkCtx = DINK_TargetRenderContext()


# contains data for rendering a single frame
class DINK_TargetRenderData:
    def __init__(self, angleDef, frameIndex, fileName):
        self.angleDef = angleDef
        self.frameIndex = frameIndex
        self.fileName = fileName


class DINK_TargetData:
    armature = None
    action = None
    rotator = None
    isValid = False

    renderFrameList = None
    isRenderFrameListValid = False

    message = None

    def clearWithMsg(self, errorMsg):
        self.armature = None
        self.action = None
        self.rotator = None
        self.isValid = False
        self.renderFrameList = None
        self.isRenderFrameListValid = False
        self.message = errorMsg


    def renderFrame(self, context, renderFrame):
        if (self.isValid == False or
            self.isRenderFrameListValid == False):
            return

        #set the armature's rotation on the Z axis accordingly
        self.rotateTargetZ(renderFrame.angleDef[1])
        # set current frame in scene
        context.scene.frame_current = renderFrame.frameIndex
        # set rendered file path
        context.scene.render.filepath = renderFrame.fileName
        # perform render
        bpy.ops.render.render( animation=False, write_still=True)

        # restore rotator to initial zero position
        # self.rotateTargetZ(0)


    def rotateTargetZ(self, angle):
        if (self.isValid == False):
            return

        self.rotator.rotation_euler[2] = math.radians(angle)

    def clearRotatorAction(self, context):
        if (self.isValid == False):
            return

        #clear the rotator animation data...
        self.rotator.animation_data.action = None

    def updateArmatureAction(self, context):
        if (self.isValid == False):
            return

        # set the armature's animation action
        self.armature.animation_data.action = self.action
        # update scene frame start / end
        context.scene.frame_start = 1
        context.scene.frame_end = int(self.armature.animation_data.action.frame_range[1])

        # make sure current frame is valid...
        if (context.scene.frame_current > context.scene.frame_end):
            context.scene.frame_current = context.scene.frame_end

    def initRenderFrameList(self, context, pathBase, fileNamePrefix, fileNameMode, dinkAngles):
        if (self.isValid == False):
            self.renderFrameList = None
            self.isRenderFrameListValid = False
            return

        # simple way to remove invalid action name characters
        actionNameSafe = self.action.name
        invalidChars = "\\/:*\"<>|"
        for ic in invalidChars:
            actionNameSafe = actionNameSafe.replace(ic,' ')

        # set rendered folder path based on base-path and action name
        self.pathAnimBase = os.path.join(pathBase, actionNameSafe)
        # make sure folder exists
        if not os.path.exists(self.pathAnimBase):
            os.makedirs(self.pathAnimBase)
        # sanity check
        if not os.path.exists(self.pathAnimBase):
            self.clearWithMsg("ERROR: Can not create path: " + pathAnim)
            return

        #
        self.renderFrameList = []

        fStart = int(self.action.frame_range[0])
        fEnd = int(self.action.frame_range[1])

        # rotate model for each Dink direction
        for angleDef in dinkAngles:
            # go through each frame
            for i in range(fStart, fEnd + 1, 1):
                outIndex = i-fStart+1;
                # prepare rendered file name
                if (fileNameMode == "prefix5"):
                    animFile = fileNamePrefix + str(angleDef[0]) + str(outIndex).zfill(2)
                else:
                    animFile = fileNamePrefix + str(angleDef[0]) + "-" + str(outIndex).zfill(2)
                # prepare rendered file path
                fileName = os.path.join(self.pathAnimBase, animFile)
                # save the rendered frame info to the list
                self.renderFrameList.append(DINK_TargetRenderData(angleDef, i, fileName))

        # frame list initialized and ready for rendering!
        self.isRenderFrameListValid = True

    def __init__(self, context, actionName):
        self.clearWithMsg("Initializing target for action: " + actionName)

        #scene
        scene = context.scene

        # find rotator parent object
        self.rotator = scene.objects.get("ModelRotator")
        # check if we found the correct rotator parent
        if (self.rotator == None):
            self.clearWithMsg("ERROR: Model rotator not found in scene, did you accidentally delete it?!")
            return

        # find action
        self.action = bpy.data.actions.get(actionName)
        # check if action is valid
        if (self.action == None):
            self.clearWithMsg("ERROR: Action named " + actionName + " was not found in scene!")
            return

        #find our model object that we want to rotate
        for x in self.rotator.children:
            if x.type == "ARMATURE" and x.hide_render == False:
                self.message = "Found armature named: " + x.name
                self.armature = x
                break
        # check if we found armature
        if (self.armature == None):
            self.clearWithMsg("ERROR: Armature not found in scene!")
            return

        # all done yay!
        self.isValid = True
        return



def DINK_GetDirectionsDict():
    return {
        1 : [315,   "SW",  "1 (SW 315°)"],
        2 : [0,     "S",   "2 ( S   0°)"],
        3 : [45,    "SE",  "3 (SE  45°)"],
        4 : [270,   "W",   "4 ( W 270°)"],
        6 : [90,    "E",   "6 ( E  90°)"],
        7 : [225,   "NW",  "7 (NW 225°)"],
        8 : [180,   "N",   "8 ( N 180°)"],
        9 : [135,   "NE",  "9 (NE 135°)"]
    }

def DINK_GetDirectionsAll():
    return [
        [1, 315, "SW",  "1 (SW 315°)"],
        [2, 0,   "S",   "2 ( S   0°)"],
        [3, 45,  "SE",  "3 (SE  45°)"],
        [4, 270, "W",   "4 ( W 270°)"],
        [6, 90,  "E",   "6 ( E  90°)"],
        [7, 225, "NW",  "7 (NW 225°)"],
        [8, 180, "N",   "8 ( N 180°)"],
        [9, 135, "NE",  "9 (NE 135°)"],
    ]

def DINK_GetDirections2468():
    return [
        [2, 0,   "S",   "2 ( S   0°)"],
        [4, 270, "W",   "4 ( W 270°)"],
        [6, 90,  "E",   "6 ( E  90°)"],
        [8, 180, "N",   "8 ( N 180°)"],
    ]

def DINK_GetDirections1379():
    return [
        [1, 315, "SW",  "1 (SW 315°)"],
        [3, 45,  "SE",  "3 (SE  45°)"],
        [7, 225, "NW",  "7 (NW 225°)"],
        [9, 135, "NE",  "9 (NE 135°)"],
    ]

# --- end of render function

# ----------------------------------------------------------------
# Register stuff
# ----------------------------------------------------------------

# defines a list of blender classes that are used in this script
classes = (
    DINK_ListItems,
    DINK_Properties,
#    DINK_TargetRenderContext,
    DINK_UL_MyList,
    DINK_OP_RefreshCompositing,
    DINK_OP_CustomRenderModal,
    DINK_OP_CustomRenderCancelModal,
    DINK_OP_RefreshActionList,
    DINK_OP_Rotate,
    DINK_PT_MyRenderPanel,
    DINK_PT_RotatePanelNodeEditor,
    DINK_PT_RotatePanelView3D
)

# --- register my classes
def register():
    if (3, 6, 0) > bpy.app.version:
        print("ERROR - Dink Blender Template is supported only in Blender 3.6 or newer!")
        return

    # clear render state...
    gDinkCtx.clear()

    if (4, 0, 0) > bpy.app.version:
        gDinkCtx.canUseCoolProgressBar = False
    else:
        gDinkCtx.canUseCoolProgressBar = True

    # register each class defined in classes
    for cls in classes:
        bpy.utils.register_class(cls)

    # DINK_Properties gets saved in the scene
    bpy.types.Scene.dinkProps = bpy.props.PointerProperty(type=DINK_Properties)

    # DINK_TargetRenderContext is temporary, register it in the window manager instead of the scene
    # using global variable for context now....
    #bpy.types.WindowManager.dinkCtx = bpy.props.PointerProperty(type=DINK_TargetRenderContext)

    # register progress bar property, also a temporary property
    bpy.types.WindowManager.dinkRenderProgress = bpy.props.FloatProperty()

    # initialize the list of actions in the DINK_Properties object
    DINK_PropsInitializeActionList(bpy.context)

    # restore enum values when initializing
    dinkProps = bpy.context.scene.dinkProps
    dinkProps.restoreEnumValues()

#    bpy.context.window_manager.dinkCtx.clear()


# --- unregister my classes
def unregister():
    if (3, 6, 0) > byp.app.version:
        # nothing was registered, nothing to do here
        return

    # unregister each class defined in classes, in reverse order
    for cls in classes.reversed():
        bpy.utils.unregister_class(cls)


    # delete type defs...

    del bpy.types.Scene.dinkProps
    # using global variable for context now....
    #del bpy.types.WindowManager.dinkCtx
    del  bpy.types.WindowManager.dinkRenderProgress

if __name__ == "__main__":
    register()
