### V1.3.2
Fixed an issue where action start/end frames were not calculated correctly in the script. Start frame was pretty much hardcoded to 1 and end frame was considered to be the total frame count. In certain situations this lead to rendering not working correctly.


### V1.3.1
Set the World's Strength property to 0.0.

Modified shadow angles to be closer to Dink's original style.

Added model rotate buttons to the Compositing editor.


### V1.3.0
Fixed grid-pattern shadow not rendering in Blender V4.2 (deja vu?). This was because they removed an existing feature for a compositing node with plans to add it back "in the future"... [https://developer.blender.org/docs/release_notes/4.2/compositor/#repetition](https://developer.blender.org/docs/release_notes/4.2/compositor/#repetition)

Replaced the direction selection checkbox with 3 buttons for rendering all directions, diagonal only (1, 3, 7, 9) and horizontal/vertical only (2, 4, 6, 8)

Added progress bar to rendering process. NOTE: In blender 3.6 there will be only progress text displayed as the progress bar is unavailable. [Note: This was actually a lot more work than I expected, hah. But I think the end result is worth it.]

Added abort button when rendering.

Fixed an issue with ModelRotator not rotating the model...

Fixed an issue with selected values for "Output Format" and "Name Format" properties not being loaded correctly when opening file.

Set the Grid object's material emission property to 0.0.


### V1.2.0
Can now set sprite image size, scale and other parameters from the Dink Render panel.

Changing properties in the Dink Render panel automatically updates compositing.

Added Dink panel in 3D viewport for rotating model.

Can now render normal shadows with transparency when saving as PNG!

### V1.1.0
A change in Blender V4.1 changed the way the compositing Crop nodes work: [https://projects.blender.org/blender/blender/commit/4bf08198a7](https://projects.blender.org/blender/blender/commit/4bf08198a7)

This lead to the shadow not rendering in V4.1 because the "DinkShadow.Crop" node had its up/down limits flipped. I switched the up/down limits to fix the issue.

### V1.0.0
Initial version...