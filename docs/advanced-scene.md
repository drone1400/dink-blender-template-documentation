![image](./assets/images/scene-layout.png)

The scene has the following objects:
### Camera
A simple Ortographic camera, positioned to give the right perspective for Dink sprites.

### GridShadowCatcher
A white plane on which the shadow gets projected. The important thing about it is that it has the `Object -> Visibility -> Mask -> Shadow Catcher` option enabled, which allows us to use Blender's Shadow Catcher feature in Compositing.

### ModelRotator
This is an empty object that serves a very important role, it is the target of the rotation modifications during the automated rendering script.

Also, its first child object of the type `ARMATURE` that is also not hidden from rendering (the `hide_render` property is `False`) will be the object whose actions will be rendered by the automatic rendering script.

In V1.2 I also added an arrow to serve as an orientation reference when adding models. The model needs to face the direction of the arrow for the sprites to render in the correct direction.

### ShadowSource
A *Sun* type light source that is positioned so that it casts a shadow at the correct angle to be consistent with other Dink sprites. Feel free to tweak its intensity and whatnot, but you should leave its position as is unless you want your model to have a shadow at a different non-standard angle.

Just like the **GridShadowCatcher** this also has the `Object -> Visibility -> Mask -> Shadow Catcher` property enabled, otherwise the shadow cast by it would not be visible in the Shadow Catcher part of Compositing.