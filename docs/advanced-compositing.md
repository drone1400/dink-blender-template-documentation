![image](./assets/images/compositing-overview.png)

The compositing tree is made up of 9 distinct groups:

1. Input 
2. Dink Shadow Grid Pattern Maker
3. Dink Shadow - Normal
4. White Background Remove
5. BMP edge filter
6. Dink Render Bmp
7. Dink Render Png
8. In Game Preview Example
9. Output

## Input
This is just a **Render Input** type node, and a few **Preview** nodes so that you can preview what the Compositing raw inputs look like.

![image](./assets/images/compositing-1-input.png)

## Dink Shadow Grid Pattern Maker
This is where the magic happens and the object's shadow becomes a grid pattern!

![image](./assets/images/compositing-2-shadow.png)

The group takes the **Shadow Catcher** data from the **Render Input** node and passes it through a **Greater Than** node. Pixels that have a color value greater than the set threshold will evaluate as `True (1.0)` and when processed as an image will be interpreted as pure white. Pixels that have a value lower than the threshold, will be interpreted as `False (0.0)` and thus be interpreted as pure black. You can see the result in the preview node.

~~A simple 2x2 pixel checker pattern BMP is then passed through a **Translate** node with wrapping enabled for both axes, this effectively creates an infinitely stretching checker pattern.~~
In Blender V4.2, the **Translate** node's repetition functionality was broken: [https://developer.blender.org/docs/release_notes/4.2/compositor/#repetition](https://developer.blender.org/docs/release_notes/4.2/compositor/#repetition)
Thus, in DBT V1.3, the checker-grid pattern image is now generated programmatically by the script when rendering.

Next, the two are added together using a **Mix Color - Add** node. The pattern gets added over the whole image, but since black has a value of 0 and white a value of 1, effectively only the white pixels added to the black shadow area do anything, effectively creating a white checker-grid pattern over the existing image.

Finally, the output of this last node is passed through a **Crop** node. Since I used that translate node with wrap enabled, later on in the **In Game Preview Example** group, the black pixels of the shadow-checker-grid would get repeated all over the place, since they still wrap outside the original image's bounds! The **Crop** node set to be relative to the image size effectively ensures that the wrapping from the previous node does not exceed the actual sprite image size! Took me a while to figure this one out...

## Dink Shadow - Normal
Added in V1.2, this group turns the **Render Input** node's Shadow Catcher layer into a transparent shadow that can be mixed with the object render.

From what I understand, the shadow catcher input is usually used by multiplying with a background image to obtain a shadow on that image.

Since the goal is to render a transparent sprite with no background, I had to convert the grayscale Shadow Catcher input into a transparent shadow. This is achieved by using the Shadow Catcher data as an alpha layer for a pure black image.

First, I use an **Invert Color** node. Then a **Greater Than** node to make a mask for filtering out undesired pixels with low values. I then **Multiply** the result of the two nodes, the result being used to set the alpha channel using a **Set Alpha** node with a pure black background.

Before passing the value to the **Set Alpha** node, I also use another **Multiply** node whose value is adjustable from GUI, to allow the user to make the shadow lighter or darker.

As with the grid pattern shadow, I use a **Crop** node at the very end.

## White Background Remove
This group takes the previous shadow image and replaces the white pixels with transparent pixels.

![image](./assets/images/compositing-4-white-remove.png)

It simply takes the final image from the shadow generating group and using a **Less Than* node and a **Set Alpha** node, sets the alpha for the white pixels to 0, making them transparent. 

This is useful so that we don't have a white background when saving as PNG or in the **In Game Preview Example** group.

## BMP edge filter

This group takes the **Alpha** and **Image** values from the **Render Input** node and makes an image with hard solid color pixels on its edges.

![image](./assets/images/compositing-5-bmp-filter.png)

First, the alpha gets passed through a **Greater Than** node; alpha that is lower than the threshold becomes 0.0 while the rest becomes 1.0.

I'm not quite sure what black magic inspired me to use the **Color Mix** node, but basically, it makes the edge pixels have a hard non-transparent color.

Finally, the new alpha value is written into the image using a **Set Alpha** node so that we get rid of the white background we added using the mix node. 

!!! note Improvements?
    I'm sure this could be improved somehow, but for now it's simple enough and it works...

## Dink Render Bmp
This node combines the edge-filtered image with the checker-grid-shadow creating the final BMP image.

![image](./assets/images/compositing-6-dink-render-bmp.png)

The model image and shadow are combined using a **Mix - Alpha Over** node. A second **Mix - Alpha Over** node is used to add a white background to the image.

## Dink Render Bmp
This node combines the **Render Input** image with the checker-grid-shadow creating the final PNG image.

![image](./assets/images/compositing-7-dink-render-png.png)

The model image and shadow are combined using a **Mix - Alpha Over** node. 

## In Game Preview Example

This group just uses **Mix - Alpha Over** nodes to overlay the BMP / PNG images over a static screenshot taken from me playing [Periculo Island](https://www.dinknetwork.com/file/periculo_island/) 

The purpose of this group is to just give you a rough idea of how the sprite looks like compared to Dink.

![image](./assets/images/compositing-8-preview.png)

## Output

Well, this is just the final output node...

![image](./assets/images/compositing-9-output.png)

The automatic rendering script changes this node's input based on BMP/PNG selection.
