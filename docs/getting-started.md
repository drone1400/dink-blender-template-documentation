## Quick render single frame
Just press F12 to render a single frame.

## Rendering the demo model with the automatic script
The file comes with a custom model by [Bluedy](https://www.dinknetwork.com/user/Bluedy) all ready to be rendered.

First, either allow the automatic execution of the blender file's internal `customRender.py` script on startup, or run it manually yourself from blender's Scripting workspace.
The script will add a custom **Dink Render** panel to the scene **Output Properties** page, and a custom **Dink** panel to the 3D Viewport for rotating the model in the 8 Dink directions.

![image](./assets/images/overview.png)

You can set the base folder where you want to save the rendered sprites by editing the **Output Path** property. By default, it's in `C:\dink_render\`

You can also select the **Output Format**, either PNG or BMP.

Simply click the **Render All Directions** button and wait for Blender to do its thing. You should have a bunch of BMP or PNG sprites rendered like so:

![image](./assets/images/example-render.png#only-light)
![image](./assets/images/example-render-dark-mode.png#only-dark)

## Previewing in Compositing

After doing a quick render with F12, you can also preview what the sprite looks like in-game in the Compositing workspace:
![image](./assets/images/overview-compositing.png)

Make sure the `Preview-DinkHD-Compare` node is selected. You can adjust the zoom in the **View** panel on the right, or press `V` / or `ALT + V` to zoom out/in.

As of V1.3.1, you can also quickly rotate the model using the custom **Dink** Panel in **Tools**. Unlike the 3D Viewport panel which just rotates the model, this one also does a quick render pass in order to refresh the compositing preview.

## Using your own model

* First, import your own armature/model and actions into the scene.
* Make sure the model is a child of the **ModelRotator** object
!!! warning Model Rotator
    This is very important, if the object is not a child of Model Rotator, the automatic rendering script won't work!
* Make sure the object is facing the direction of the arrow just like the example model, otherwise your model will be staring off the wrong way when its rotations are generated.
* Delete or **Disable in renders** the old example model in the scene

!!! note
    The automatic rendering script will use the first Armature that is a child of **ModelRotator** and not hidden from rendering.        

* After importing/creating animation actions, hit the **Refresh Action List** button in the **Dink Render** properties panel. Uncheck the actions you don't want to render.
* Set your desired sprite size by changing the **Dink Render** properties `Sprite Size X/Y` and `Sprite Ortographic Scale` properties.

!!! note
    The `Sprite Ortographic Scale` property sets the **Camera**'s Lens Ortographic Scale property. The higher the value, the more zoomed out the sprite will be.

That's it, you should be good to go now!

You can also check out the [example custom model](importing-model-example.md) import page.

## Adjusting Lighting
You can add/remove extra light sources and tweak other lighting parameters, but you should leave the `ShadowSource` light's position as is, since it is responsible for casting the shadow used to generate the classic Dink grid shadow.

If you add extra light sources to the scene and don't want them casting shadows that interfere with the sprite's shadow, be sure to disable the light sources' `Cast Shadow` property!

Since V1.3.1, the World's Strength is set to 0.0 by default. You can adjust this in the Properties Panel on the right, under `World->Surface`

Changing the GridShadowCatcher plane's `Object->Surface->BaseColor` or tweaking its `Object->Emission` properties can also affect lighting. This will also affect shadows and you may need to adjust some of the filter parameters in the Dink Render settings.


## Other considerations
Feel free to tweak things to your liking, but keep in mind that you should leave the compositing nodes as they are, or it can break the automatic rendering script! Only mess with that if you know what you're doing!

For more info, check the [Scene Configuration](advanced-scene.md) page.