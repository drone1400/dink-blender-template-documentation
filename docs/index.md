# DBT - Dink Blender Template

![image](./assets/images/overview.png)

This is a Blender template for automatically rendering sprites for Dink Smallwood. It automatically generates the classic Dink grid shadow pattern using Blender's compositing features and includes a script for automatically rendering animations for Dink's 8 directions.

It is made for Blender 4, but I think it should run on Blender 3.6 too.

Scripting and compositing by [drone1400](https://www.dinknetwork.com/user/drone1400), example 3D model and rigging by [Bluedy](https://www.dinknetwork.com/user/Bluedy)

Inspired by [iplaydink's Dink Shadows for Blender](https://www.dinknetwork.com/file/dink_shadows_for_blender/)

Big thanks to [SimonK](https://www.dinknetwork.com/user/SimonK/) for all the feedback and helping improve this blender template!

You can also check out his videos on using the Dink Blender Template on YouTube:

- [Intro to using Dink Blender Template](https://www.youtube.com/watch?v=7sjjmYdckbs)
- [Dink Blender Template update to v1.2](https://www.youtube.com/watch?v=9bfgmjqeJ_k)


### Getting the blender file
The blender template itself is hosted over on The Dink Network: [https://www.dinknetwork.com/file/dink_blender_template/](https://www.dinknetwork.com/file/dink_blender_template/)

### How to use
Head over to the [Getting Started](getting-started.md) page...